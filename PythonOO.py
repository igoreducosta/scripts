class Conta:
    def __init__(self,numero):
        self.numero = numero
        self.saldo= 0.0
        
    def consultar_saldo(self):
        return self.saldo
        
    def creditar(self,valor):
        self.saldo += valor
        
    def debitar (self,valor):
        self.saldo -=valor
        
    def transferir(self,conta,valor):
        self.saldo -=valor
        conta.saldo += valor

class Poupanca(Conta):
    def __init__(self,numero):
        super().__init__(numero)
        self.__redimento = 0.0
        
    def consultar_redimento(self):
        return self.__rendimento
        
    def gerar_rendimento(self,taxa):
        self.__redimento += super().consultar_saldo * taxa /100
        
conta = Poupanca(1)
conta.creditar(200)
conta.gerar_rendimento(10)
print(conta.consultar_saldo())
print(conta.consultar_redimento())
