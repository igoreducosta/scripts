from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
from matplotlib import pyplot as plt
import numpy as np
from datetime import datetime
from tensorflow.examples.tutorials.mnist import input_data # download da biblioteca


mnist = input_data.read_data_sets('MNIST_data') 


def dense_layer(X, n_neurons, name, activation=None):
    with tf.name_scope(name):
        n_inputs = int(X.get_shape()[1])
        initializer = tf.contrib.layers.xavier_initializer()
        init=tf.truncated_normal((n_inputs,n_neurons), stddev=2/np.sqrt(n_inputs))
        w = tf.Variable(init, name="weights") # pesos
        b = tf.Variable(tf.zeros([n_neurons]), name = "biases") # vies
        z = tf.matmul(X, w) + b
        if activation:
            return tf.nn.relu(z) # metodo para ativar a funçao ReLU(funcao de ativaçao Y = f((w*x)+b)
        else:
            return z

# Define os placeholders de entrada e saída
X=tf.placeholder(tf.float32,(None,28*28),name="X")
y=tf.placeholder(tf.int64,shape=(None),name="y")


# Cria a rede utilizando a função definida anteriormente
with tf.name_scope("dnn"):
    layer1= dense_layer(X, 300, "hidden1", activation=True)
    layer2= dense_layer(layer1, 100, "hidden2", activation=True)
    logits= dense_layer(layer2, 10, "logits")

# Define a os parâmetros de perda
with tf.name_scope("loss"):
    xentropy=tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
    loss=tf.reduce_mean(xentropy,name="loss")

# Cria um otimizador e parametriza um operador para esse otimizador
learning_rate = 0.01
with tf.name_scope("train"):
    optimizer=tf.train.GradientDescentOptimizer(learning_rate)
    training_op=optimizer.minimize(loss)
# Cria as variáveis de avaliação
with tf.name_scope("eval"):
    correct= tf.nn.in_top_k(logits, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
    conf = tf.reduce_max(tf.nn.softmax(logits, axis=1), axis=1)

    tf.summary.scalar("Accuracy",accuracy)

init = tf.global_variables_initializer()
saver = tf.train.Saver()
merged = tf.summary.merge_all()

n_epochs = 3
batch_size = 50

with tf.Session() as sess:
    # Cria os arquivos do tensorboard
    now = datetime.utcnow()
    train_writer = tf.summary.FileWriter('logdir/train-{}'.format(now),sess.graph)
    test_writer = tf.summary.FileWriter('logdir/test-{}'.format(now))
    
    # Inicia as variáveis do tensorflow
    init.run()
    
    for epoch in range(n_epochs):
        flag = 1
        # Itera n baches do dataset
        for iteration in range(mnist.train.num_examples//batch_size):
            X_batch, y_batch=mnist.train.next_batch(batch_size)
            sess.run(training_op,feed_dict={X:X_batch,y:y_batch})
            
        # Adiciona as métricas para o Tensorboar
        train_writer.add_summary(merged.eval(feed_dict={X:X_batch,y:y_batch}),epoch)
        test_writer.add_summary(merged.eval(feed_dict={X:mnist.test.images,y:mnist.test.labels}),epoch)

        # Calcula métricas de acurácia para acompanhar o treinamento
        acc_train=accuracy.eval(feed_dict={X:X_batch,y:y_batch})
        acc_test=accuracy.eval(feed_dict={X:mnist.test.images,y:mnist.test.labels})
        print(epoch+1,"Train accuracy:",acc_train,"Test accuracy:",acc_test)
        
        # Salva os checkpoits por cada Época
        save_path=saver.save(sess,"./logdir/my_model_final.ckpt")
r = 's'
while r != 'n':
    batch_xs, batch_ys = mnist.test.next_batch(1)
    with tf.Session() as sess:
        saver.restore(sess, "./logdir/my_model_final.ckpt")
        X_new_scaled=batch_xs
        Z = logits.eval(feed_dict={X: X_new_scaled})
        y_pred=np.argmax(Z, axis=1)
        confidence = sess.run(tf.nn.softmax(Z, axis=1))
        print ("Palpite: {} | Confiança: {:.2f}%".format(y_pred[0],confidence[0][y_pred][0] * 100))
        two_d = (np.reshape(batch_xs, (28, 28)) * 255).astype(np.uint8)
        plt.imshow(two_d, 'gray',interpolation='nearest')
        plt.show()
        r = str(input('Deseja continuar [s/n]: ')).lower().strip()

